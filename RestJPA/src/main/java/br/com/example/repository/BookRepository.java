//

package br.com.example.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import br.com.example.entity.Book;

/**
 *
 */
@Component
public interface BookRepository extends CrudRepository<Book, Integer> {
    public Book findByTitle(String title);
}
