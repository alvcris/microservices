package br.com.example.controller;

import br.com.example.entity.Book;
import br.com.example.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Optional;

/**
 *
 */
@RestController
public class RestJPAController {

    @Autowired
    private BookService service;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    /**
     *
     * @param title
     * @param pages
     * @param author
     * @return
     */
    @PostMapping(value = "/book")
    public ResponseEntity<Book> addBook(
            @RequestHeader("title") final String title,
            @RequestHeader("pages") final Integer pages,
            @RequestHeader("author") final String author)
    {
        Optional<Book> book = service.create(title, pages, author);
        LOGGER.info("" + book);
        return book.map(response -> ResponseEntity.ok().body(response))
                .orElse(ResponseEntity.status(HttpStatus.CONFLICT).body(null));
    }

    /**
     *
     * @param book
     * @return
     */
    @PostMapping(value = "/book", headers = {"content-type=application/json"})
    public ResponseEntity<Book> addBookPayload(@RequestBody Book book)
    {
        Optional<Book> createdBook = service.create(book);
        LOGGER.info("" + book);
        return createdBook.map(response -> ResponseEntity.ok().body(response))
                .orElse(ResponseEntity.status(HttpStatus.CONFLICT).body(null));
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/book/{id}")
    public ResponseEntity<Book> getBook(@PathVariable Integer id)
    {
        Optional<Book> book = service.getById(id);
        return book.map(response -> ResponseEntity.ok().body(response))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    /**
     *
     * @return
     */
    @GetMapping(value = "/book")
    public ResponseEntity<Collection<Book>> getBooks()
    {
        Optional<Collection<Book>> books = service.getBooks();
        return books.map(response -> ResponseEntity.ok().body(response))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    /**
     *
     * @param id
     * @param title
     * @param pages
     * @param author
     * @return
     */
    @PutMapping(value = "/book/{id}")
    public ResponseEntity<Book> updateBook(
            @PathVariable Integer id,
            @RequestHeader("title") final String title,
            @RequestHeader("pages") final Integer pages,
            @RequestHeader("author") final String author)
    {
        Optional<Book> book = service.updateById(id, title, pages, author);
        return book.map(response -> ResponseEntity.ok().body(response))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    @DeleteMapping(value = "/book/{id}")
    public ResponseEntity<Book> deleteBook(@PathVariable Integer id)
    {
        try {
            service.deleteBook(id);
        } catch (Exception e) {
            //do nothing
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }
}
