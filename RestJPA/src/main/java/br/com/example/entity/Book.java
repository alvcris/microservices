package br.com.example.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 */
@Entity
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String title;

    @Column
    private Integer pages;

    @Column
    private String author;

    /**
     *
     */
    public Book() {
        //empty one
    }

    /**
     * @param id
     * @param title
     * @param pages
     * @param author
     */
    public Book(final Integer id, final String title, final Integer pages, final String author) {
        this.id = id;
        this.title = title;
        this.pages = pages;
        this.author = author;
    }

    /**
     * @param title
     * @param pages
     * @param author
     */
    public Book(final String title, final Integer pages, final String author) {
        this.title = title;
        this.pages = pages;
        this.author = author;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the pages
     */
    public Integer getPages() {
        return pages;
    }

    /**
     * @param pages the pages to set
     */
    public void setPages(final Integer pages) {
        this.pages = pages;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(final String author) {
        this.author = author;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pages=" + pages +
                ", author='" + author + '\'' +
                '}';
    }
}
