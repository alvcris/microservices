package br.com.example.services;

import br.com.example.entity.Book;
import br.com.example.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 *
 */
@Service
public class BookService {

    @Autowired
    private BookRepository repo;

    /**
     *
     * @param title
     * @param pages
     * @param author
     * @return
     */
    public Optional<Book> create(final String title, final Integer pages, final String author) {

        if (repo.findByTitle(title) == null) {
            return Optional.of(repo.save(new Book(title, pages, author)));
        }
        return Optional.ofNullable(null);
    }

    /**
     *
     * @param book
     * @return
     */
    public Optional<Book> create(final Book book) {

        if (repo.findByTitle(book.getTitle()) == null) {
            return Optional.of(repo.save(book));
        }
        return Optional.ofNullable(null);
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<Book> getById(Integer id) {
        return Optional.ofNullable(repo.findOne(id));
    }

    /**
     *
     * @return
     */
    public Optional<Collection<Book>> getBooks(){
        List<Book> books = new ArrayList<>();
        repo.findAll().forEach(books::add);

        return Optional.ofNullable(books);
    }

    /**
     *
     * @param id
     * @param title
     * @param pages
     * @param author
     * @return
     */
    public Optional<Book> updateById(Integer id, String title, Integer pages, String author) {
        Book book = repo.findOne(id);

        if(book != null) {
            if (!book.getTitle().equals(title)) {
                book.setTitle(title);
            }

            if (book.getPages() != pages) {
                book.setPages(pages);
            }

            if (!book.getAuthor().equals(author)) {
                book.setAuthor(author);
            }
        }
        return Optional.ofNullable(book);
    }

    /**
     *
     * @param id
     */
    public void deleteBook(Integer id) {
        repo.delete(id);
    }
}
