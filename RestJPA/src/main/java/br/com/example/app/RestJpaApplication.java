package br.com.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("br.com.example")
@EntityScan("br.com.example.entity")
@EnableJpaRepositories("br.com.example.repository")
public class RestJpaApplication {

    public static void main(final String[] args) {
        SpringApplication.run(RestJpaApplication.class, args);
    }
}
