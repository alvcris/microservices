package br.com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.example.api.Registration;
import br.com.example.model.Client;
import br.com.example.service.RegistrationService;

@RestController
@RequestMapping(value = "/registration")
public class RegistrationImpl implements Registration
{
    @Autowired
    private RegistrationService service;

    @Override
    @GetMapping(value = "/client")
    public ResponseEntity<Client> getClient(@RequestHeader("name") final String name, @RequestHeader("age") final String age)
    {
        return ResponseEntity.status(HttpStatus.OK).body(service.getClient(name, age).get());
    }

    @Override
    @GetMapping(value = "/client/entity")
    public Client getClientEntity(@RequestHeader("name") final String name, @RequestHeader("age") final String age)
    {
        return service.getClient(name, age).get();
    }

}
