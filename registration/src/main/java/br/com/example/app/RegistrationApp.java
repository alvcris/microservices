package br.com.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan("br.com.example")
public class RegistrationApp
{
    public static void main(final String[] args)
    {
        SpringApplication.run(RegistrationApp.class, args);
    }
}
