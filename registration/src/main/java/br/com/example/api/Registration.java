package br.com.example.api;

import org.springframework.http.ResponseEntity;

import br.com.example.model.Client;

public interface Registration
{
    public ResponseEntity<Client> getClient(String name, String age);

    public Client getClientEntity(String nome, String idade);
}
