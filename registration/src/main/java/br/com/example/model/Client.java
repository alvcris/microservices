package br.com.example.model;

public class Client
{
    private String name;
    private String age;

    public Client()
    {
        //empty constructor

    }

    public Client(final String name, final String age)
    {
        this.name = name;
        this.age = age;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name)
    {
        this.name = name;
    }

    /**
     * @return the age
     */
    public String getAge()
    {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(final String age)
    {
        this.age = age;
    }

}
