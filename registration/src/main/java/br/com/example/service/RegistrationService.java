package br.com.example.service;

import org.springframework.stereotype.Component;

import com.google.common.base.Optional;

import br.com.example.model.Client;

/**
 *
 */
@Component
public class RegistrationService
{

    /**
     * @param name
     * @param age
     * @return
     */
    public Optional<Client> getClient(final String name, final String age)
    {
        return Optional.of(new Client(name, age));
    }

}
