package br.com.example.app;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import br.com.example.model.Client;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegistrationImplTest
{
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getClientTest()
    {
        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.add("name", "myName");
        headers.add("age", "10");

        final HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        final ResponseEntity<Client> response = restTemplate.exchange("/registration/client", HttpMethod.GET, entity, Client.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("10", response.getBody().getAge());
        assertEquals("myName", response.getBody().getName());
    }
}
