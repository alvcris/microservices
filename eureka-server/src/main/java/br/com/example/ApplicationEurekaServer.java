package br.com.example;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ApplicationEurekaServer
{

    public static void main(final String[] args)
    {
        new SpringApplicationBuilder(ApplicationEurekaServer.class).web(true).run(args);
    }
}
